# alison-rocker-tilt-sound

## wav files are:

```
% ffmpeg -i $file
stereo, 16 bit wav, 44.1kHz, no metadata
ffmpeg reports:
Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), 44100 Hz, stereo, s16, 1411 kb/s
```

On linux (maybe Apple?), install ffmpeg and use
```
./to-16bitwav $in
or more manually:
ffmpeg -i "$in" -ar 44100  -c:a pcm_s16le -map_metadata:g -1 -fflags +bitexact `echo $in | sed 's/-24/-16/'`
```

Name them with 3 leading digits, 001..006, e.g.

    001-birds.wav

And copy to sd-card in "WAV Trigger" board.

**NB** Sounds are played in a loop, so make sure the end will flow into the start. I.e. both at 0. Otherwise you get a click.

## Wiring & Use

Multi-strand.
20gauge or 22gauge (american), which is 0.5mm^2, 0.38mm^2, or 0.34mm^2 supposedly.

See the fritzing for wiring, breadboard view.

See (rocker-tilt-sound/rocker-tilt-sound.ino)[rocker-tilt-sound/rocker-tilt-sound.ino], since I like to keep info near code-use.

Summary:

Each rocker normally-open, +5v closed, to a digital pin. Needs pull-down (10k+).

Pot on A0 for background-volume.

Red-LED near reset-button is "Heartbeat": slow-ish == good, fast-ish means wrong-number-of-tracks.

LED-bar shows when a tilt-switch is closed. Useful for debugging wiring to rockers.


