/*
  Gold Dust Rocking Chair
  i.e. tilt detect -> Robertsonics wav-trigger (aka sparkfun wig 36660)
  has a fritzing

  background-volume pot on A0
  See below for tilt-switches on digitals
  Heartbeat: slow-ish == good, fast-ish means wrong-number-of-tracks

  Have to poll, not enough "external interrupt" pins

  //    Arduino           WAV Trigger
  //    ===           ===========
  //    GND  <------> GND
  //    Rx <------> RX
  //    Tx <------> TX
  //
  //    To power the WAV Trigger from the Arduino, then close the 5V
  //    solder jumper on the WAV Trigger and connect a 4th wire:
  //
  //    USB  <------> 5V, since we can normally sink more than source, and long wires

*/

#include <Every.h>
#include <Streaming.h> // Streaming.h - supporting the << streaming operator, Mikal Hart

// NB: edited .h to use Serial1:
#include "wavTrigger.h" // copied from https://github.com/robertsonics/WAV-Trigger-Arduino-Serial-Library
#include "array_size.h"

int t_ct = 0;
const int volume_pot = A0; // global background volume. foreground volume is "max" line level

wavTrigger wTrig;
const int silent = -70;
int background_volume = -50; // when not playing, adjustable by pot on A0

class TiltToSound {
  public:

    const int track; // each rocker has a track 1..n
    const int pin; // "action" pin, each rocker has a tilt-switch

    boolean was = LOW; // N.O., closed to +5v

    Timer on_expired = Timer(1500); // like debounce for a rocker motion, play at least this long after each motion detect
    Timer has_faded = Timer(1000); // how long it takes to fade to background

    TiltToSound(int track, int pin) : track(track), pin(pin) {}

    void begin() {
      Serial << F("Setup [") << track << F("] pin ") << pin << endl;
      pinMode( pin, INPUT );

      // continous play=="loop"
      wTrig.trackLoop(track, true);
      //wTrig.trackLoop(track, false);
      // Tracks start as "playing" == background (pot will set volume almost immediately)
      wTrig.trackGain(track, background_volume / 2);
      wTrig.trackFade(track, background_volume, 500, false);
      // and they play on top of each other
      wTrig.trackPlayPoly(track);
    }

    void run() {
      boolean v = digitalRead( pin );

      if ( v != was ) {
        // any change == action
        t_ct += 1; // debug global count

        if ( on_expired.running ) {
          // keep playing
          on_expired.reset(); // restart-timer
        }
        else {
          // fade back in and play
          wTrig.trackFade(track, 0, 500, false);
          on_expired.reset();
          Serial << F("    UP ") << track << endl;
          Serial << F("    should end  ") << millis() << F(" ") << track << F(" in ") << on_expired.interval << endl;

        }

        was = v;
      }

      else if ( on_expired() ) {
        // no more "action"
        // start fade
        Serial << F("    DOWN ") << millis() << F(" ") << track << endl;
        wTrig.trackFade(track, background_volume, has_faded.interval, false); // fade out
        has_faded.reset();
      }
      else if ( has_faded() ) {
        // done fading
        Serial << F("    DONE ") << millis() << F(" ") << track << endl;
      }
      // else has_faded.running: keep fading, i.e. nothing to do yet
    }

};

/*
   can't use interrupts, only works for "external interrupt" pins, "pcint" pins only do "change" and aren't implemented

  static_assert( digitalPinToInterrupt(1) != -1, "Not ISR'able");
  static_assert( digitalPinToInterrupt(2) != -1, "Not ISR'able");
  static_assert( digitalPinToInterrupt(3) != -1, "Not ISR'able");
  //static_assert( digitalPinToInterrupt(4) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(5) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(6) != -1, "Not ISR'able"); // x
  static_assert( digitalPinToInterrupt(7) != -1, "Not ISR'able");
  //static_assert( digitalPinToInterrupt(8) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(9) != -1, "Not ISR'able");
  //static_assert( digitalPinToInterrupt(10) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(11) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(12) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(13) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(14) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(15) != -1, "Not ISR'able"); // x
  //static_assert( digitalPinToInterrupt(13) != -1, "Not ISR'able"); // x
*/

// The list of tilt-switch-pins (.pin) to wav-trigger-inputs (.track)
// so we can iterate
TiltToSound * tilts[] = { // about 90musec to read / check all
  // (track, pin)
  new TiltToSound(1, 2),
  new TiltToSound(2, 3),
  new TiltToSound(3, 5),
  new TiltToSound(4, 7),
  new TiltToSound(5, 9),
  new TiltToSound(6, 10)
};

int wav_tracks = 0;

void setup() {
  // Standard block to wait for serial on "USB CDC", aka native usb, e.g. samd21 etc's
  // if not plugged in to usb, continue eventually
  // This also gives you 3 seconds to upload if you program has a hard-hang in it
  static Every serial_wait(3 * 1000); // how long before continuing
  static Every fast_flash(50);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  // always wait, assume serial will work after serial_wait time
  while (! (Serial || serial_wait()) ) {
    if (fast_flash()) digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
    delay(10); // the delay allows upload to interrupt
  }
  Serial.println(F("\nStart " __FILE__ " " __DATE__ " " __TIME__));

  pinMode(volume_pot, INPUT);

  while ( millis() < 1000 ) {
    delay(10);  // give WAV time to reset. nb. millis from power on, so usually no wait here!
  }
  // need to reset everything, because old settings (e.g. loop) do NOT get reset by .start()
  wTrig.start();
  wTrig.setAmpPwr(false);
  wTrig.stopAllTracks(); // in case it was playing
  wTrig.samplerateOffset(0);
  wTrig.masterGain(0);
  wTrig.setReporting(true); // have todo this to let getVersion() to work
  delay(100); // examples say to give a moment

  wav_tracks = wTrig.getNumTracks();
  static char wav_version[VERSION_STRING_LEN + 1];
  Serial << F("WAV version ") << ( wTrig.getVersion(wav_version, VERSION_STRING_LEN) ? wav_version : "n/a" ) << endl;
  Serial << F("Tracks ") << wav_tracks << endl;

  // init the tilt-switch systems
  for (auto x : tilts) {
    x->begin();
  }

  // Serial << F("ledbuiltin " ) << LED_BUILTIN << endl;
  Serial << F("start loop ") << millis() << endl << endl;
}

void loop() {
  static Every print_status(200);
  static Every::Pattern heartbeat;
  static Every error_pattern(50);

  static int volume_pot_v = 0;

  static unsigned long loop_duration = 0;
  unsigned long start = micros();

  wTrig.update(); // probably ~ 1/msec at most?

  // get background volume
  static float volume_pot_v_exp_w = 10;
  volume_pot_v = (volume_pot_v_exp_w - 1) / volume_pot_v_exp_w * volume_pot_v + 1 / volume_pot_v_exp_w * analogRead( volume_pot );
  background_volume = map(volume_pot_v, 100, 900, -70, 0 );

  // poll digital pins & act on them
  for (auto x : tilts) {
    x->run();
    if ( !x->has_faded.running && ! x->on_expired.running ) {
      // if the volume pot is moved, need to update the current (background-volume
      // BUT not while playing/fading for a Particular track
      // (this also happens on the first loop to set the volume away from the init-default)
      wTrig.trackGain(x->track, background_volume);
    }
  }

  // Heartbeat: slow-ish == good, fast-ish means wrong-number-of-tracks
  if (wav_tracks == 0 && error_pattern() ) {
    digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
  }
  else if ( heartbeat() ) {
    digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
  }

  // debug status/info
  if (print_status() ) {

    if (false) {
      Serial << F("background db ") << background_volume;
      Serial << F(" Tilts (") << loop_duration << F(")musec : ") << t_ct << F(" ");
      for (auto x : tilts) {
        if ( x->on_expired.running ) {
          Serial << F("*");
        }
        else {
          Serial << F("-");
        }
      }

      Serial << F("  Tracks: ");
      for (auto x : tilts) {
        Serial << wTrig.isTrackPlaying(x->track);
      }
      Serial << endl;
    }
  }

  static const float exp_w = 10.0;

  loop_duration = (exp_w - 1) / exp_w * loop_duration + 1 / exp_w * (micros() - start);
  delay(1); // gaurantee at least some delay so native-usb can get interrupted for uploading

}
